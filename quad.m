function f = quad(x)

A = [  0.7  0.3
       0.1  0.6  ];
b = [  0.5; -0.2  ];
c = 3;

f = x * A * x' / 2 + b' * x' + c;