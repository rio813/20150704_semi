function g = quad_grad(x)

A = [  0.7  0.3
       0.1  0.6  ];
b = [  0.5; -0.2  ];
c = 3;

g = A * x' + b;