function alpha = GSM(myfun,x,s,b)
% 黄金分割法 (Golden Section Method)
%  The function is for line search in optimization
% function alpha = GSM(myfun,x,s,b)

% set constants
a     = 0;
gamma = 0.61803398874989;
l     = 1.0E-08;


% step1
alpha1 = a + ( 1.0 - gamma ) * ( b - a );
alpha2 = a + gamma * ( b - a );
phi1   = myfun(x + alpha1 * s);
phi2   = myfun(x + alpha2 * s);

while true

  % step2
  if b - a < l
    alpha = ( a + b ) / 2;
    return;
  end

  % step3
  if phi1 > phi2
    a      = alpha1;
    alpha1 = alpha2;
    phi1   = phi2;
    alpha2 = a + gamma * ( b - a );
    phi2   = myfun(x + alpha2 * s);
  else
    b      = alpha2;
    alpha2 = alpha1;
    phi2   = phi1;
    alpha1 = a + ( 1.0 - gamma ) * ( b - a );
    phi1   = myfun(x + alpha1 * s);
  end

end