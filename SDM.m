function [x_opt, f_opt, k] = SDM(myfun,mygrad,x0)
% Steepest Descent Method (SDM)
%  [x_opt, f_opt, k] = SDM(myfun,mygrad,x0)

% set constants
x         = x0;
k         = 0;
epsilon   = 1e-5;
% alpha_max = 20;

while true

  % step 1
  f = myfun(x);
  g = mygrad(x);
  if norm(g) < epsilon
    x_opt = x;
    f_opt = f;
    return;
  end

  % step 2
  % alpha = GSM(myfun, x, -g', alpha_max)
  alpha = 0.1;
  x     = x - alpha * g';
  k     = k + 1;

end